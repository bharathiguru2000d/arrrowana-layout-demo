import { Box, Grid, Typography } from "@mui/material";
import One from "../utilities/sec-1.jpg";
import Two from "../utilities/sec-2.jpg";
import Three from "../utilities/sec-3.jpg";
import Four from "../utilities/sec-4.jpg";
import Five from "../utilities/sec-5.jpg";


import  "./mainHero.css";

const MainHero = () => {
  return (
    <Grid
      container
      display={"flex"}
      justifyContent={"space-around"}
      alignItems={"center"}
      height={"90vh"}
      my={5}
      p={5}
    >
        <Grid item md={4}  className={"image-container"}  display={"flex"} justifyContent={"center"} position={'relative'} >
         
           <img src={Two} className={"movingImg1"} alt={"hero-img"} />
           <img src={Three} className={"movingImg2"} alt={"hero-img"}/>
           <img src={Four} className={"movingImg3"} alt={"hero-img"}/>
           <img src={Five} className={"movingImg4"} alt={"hero-img"}/>
            <Box component={"img"} position={"relative"}  src={One} sx={{ zIndex : 122, width : '550px' , height : "350px", objectFit : "cover" ,borderRadius :"20px" }} ></Box>
        </Grid>
      <Grid item md={4}>
        <Typography variant="h4" fontWeight={"bold"} fontSize={40} >
          We aims at developing innovative, cost effective and {"  "}
          <span style={{color:"#086AD8" , fontSize: "60px" }} >
          Secure End to End
          Technology
            </span>  solutions providing the best performance.
        </Typography>
        <Box my={2} >
          <Typography variant="subtitle1">
            Arowana provides you with digital marketing to fulfill your needs
            and we process everything in a defined manner so that to make a
            result in better customer acquisition and transformation.
          </Typography>
          <Typography variant="subtitle1" my={2}>
            Arowana will always strive to provide the best consulting services
            to the customers for the rapid growth of small to large companies.
          </Typography>
          <Typography variant="subtitle1">
          We ensure you that we deliver a quality software and services with better architecture, design, implementation and testing of quality assurance and we also include reliability and security provided to your applications.
          </Typography>
        </Box>
      </Grid>
      
    </Grid>
  );
};

export default MainHero;
