import { Box, Button, Grid, MenuItem, Paper, Select, TextField, Typography } from "@mui/material";
import Img from "../utilities/hero-form.png";
import StarIcon from '@mui/icons-material/Star';
import PhoneOutlinedIcon from '@mui/icons-material/PhoneOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';

const Contact = () => {
  return (
    <Box sx={{ height: "90vh" ,position:"relative" }}>
      <Grid
        container
        sx={{
          backgroundImage: `url(${Img})`,
          height: "100%",
          backgroundRepeat: "no-repeat",
          backgroundSize : "center",
          zIndex : 2,
          color :"white",
          display : "flex" ,
          justifyContent :"center",
          alignItems : "center",
          "&::before": {
            content: '""',
            position: "absolute",
            top: 0,
            right: 0,
            bottom: 0,
            zIndex : -1,
            left: 0,
            backgroundColor: "blue",
            opacity : .8
          },
        }}
      >
        <Grid item  xs={7} display={"flex"} justifyContent={"space-between"} alignItems={"center"} >    
            <Box textAlign={"center"} >
                <Typography variant="h3" fontWeight={"bold"} >4.9/5.0</Typography>
                <Box display={"flex"} justifyContent={"center"} >
                <StarIcon sx={{color : "yellow" ,mx:1 }}  /> 
                <StarIcon sx={{color : "yellow" ,mx:1 }}  /> 
                <StarIcon sx={{color : "yellow" ,mx:1 }}  /> 
                <StarIcon sx={{color : "yellow" ,mx:1 }}  /> 
                <StarIcon sx={{color : "yellow" ,mx:1 }}  /> 
                </Box>
                <Typography variant="subtitle2" mt={2} mb={10   } >by 700+ customers for 3200+ clients</Typography>
                <Box display={"flex"} justifyContent={"flex-start"} my={4} alignItems={"center"}  >
                    <PhoneOutlinedIcon sx={{fontSize : "50px" ,mr:2 }} />
                    <Box textAlign={"start"} >
                        <Typography variant="h4" >Call for Advice Now</Typography>
                        <Typography variant="h4" color={"burlywood"} fontWeight={"bold"} >879 053 5568</Typography>
                    </Box>
                </Box>
                <Box display={"flex"} justifyContent={"flex-start"} my={4} alignItems={"center"}  >
                    <EmailOutlinedIcon sx={{fontSize : "50px" ,mr:2 }} />
                    <Box textAlign={"start"} >
                        <Typography variant="h4" >Say Hello !</Typography>
                        <Typography variant="h4" fontWeight={"bold"} color={"burlywood"}>info@arowanasoft.com</Typography>
                    </Box>
                </Box>
            </Box>
            <Box component={Paper} width={"600px"} height={"500px"} textAlign={"center"} p={3} >
                <Typography variant="h4" fontWeight={"bold"} >Let's help You</Typography>
                <Typography variant="subtitle2" color={"gray"} mt={2} mb={4} >It's out pleasure to have a chance to cooperate.</Typography>
                <Box>
                    <Grid container my={3} >
                        <Grid item container display={"flex"} justifyContent={"space-between"} >
                            <Grid item xs={5.6} > 
                            <TextField fullWidth  placeholder="Name*"/>
                            </Grid>
                            <Grid item xs={5.6} >
                            <TextField fullWidth  placeholder="Email*"/>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} mt={2} textAlign={"start"} color={"gray"} >
                            <Select fullWidth  value={1} >
                                <MenuItem sx={{color:"gray"}} value={1} >Select1</MenuItem>
                                <MenuItem>Select2</MenuItem>
                                <MenuItem>Select3</MenuItem>
                            </Select>
                        </Grid>
                        <Grid item xs={12} mt={2} >
                            <TextField fullWidth  placeholder="let us know what you need" multiline rows={4} />
                        </Grid>
                    </Grid>
                    <Button variant="contained" size="large" mt={2}>Send Message</Button>
                </Box>
            </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Contact;
