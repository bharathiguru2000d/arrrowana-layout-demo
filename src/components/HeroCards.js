import { Box, Paper, Typography } from "@mui/material";
// import Slider from "react-slick";
// import React from "react";

// const HeroCards = () => {
//   const settings = {
//     dots: true,
//     infinite: false,
//     speed: 500,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     initialSlide: 0,
//     responsive: [
//       {
//         breakpoint: 1024,
//         settings: {
//           slidesToShow: 3,
//           slidesToScroll: 3,
//           infinite: true,
//           dots: true,
//         },
//       },
//       {
//         breakpoint: 600,
//         settings: {
//           slidesToShow: 2,
//           slidesToScroll: 2,
//           initialSlide: 2,
//         },
//       },
//       {
//         breakpoint: 480,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//         },
//       },
//     ],
//   };

//   return (
//     <Box my={2}>
//       <Typography
//         variant="h4"
//         fontWeight={"bold"}
//         color={"#086AD8"}
//         textAlign={"center"}
//       >
//         Why Choose Us
//       </Typography>
//       <Slider {...settings}>
//         <Box
//           sx={{ width: 200, p: 2, height: 256 }}
//           component={Paper}
//           elevation={1}
//         >
//           <Box
//             component={"img"}
//             sx={{ "&:hover": { transform: "transtaley(-30px)" } }}
//             src={"https://arowanasoft.com/img/icon-1.png"}
//           ></Box>
//           <Typography variant="h5" my={2}>
//             Experience
//           </Typography>
//           <Typography variant="subtitle1">
//             Our team has gained a wealth of knowledge and skills through the
//             process of digitally transforming over 600 offshore platforms and
//             more than 75 onshore industrial facilities.
//           </Typography>
//         </Box>
//         <Box
//           sx={{ width: 200, p: 2, height: 256 }}
//           component={Paper}
//           elevation={3}
//         >
//           <Box
//             component={"img"}
//             src={"https://arowanasoft.com/img/icon-2.png"}
//           ></Box>
//           <Typography variant="h5">Complete Digital Twinning</Typography>
//           <Typography variant="subtitle1">
//             Our team is proficient in assisting with digital transformation
//             efforts through in-house data acquisition, 3D modeling, data mining,
//             and asset tagging.
//           </Typography>
//         </Box>
//         <Box
//           sx={{ width: 200, p: 2, height: 256 }}
//           component={Paper}
//           elevation={3}
//         >
//           <Box
//             component={"img"}
//             src={"https://arowanasoft.com/img/icon-3.png"}
//           ></Box>
//           <Typography variant="h5">Local Presence</Typography>
//           <Typography variant="subtitle1">
//             We are a global company with a presence in many regions, which
//             allows us to offer shorter turnaround times and maximum savings to
//             our clients.
//           </Typography>
//         </Box>
//         <Box
//           sx={{ width: 200, p: 2, height: 256 }}
//           component={Paper}
//           elevation={3}
//         >
//           <Box
//             component={"img"}
//             src={"https://arowanasoft.com/img/icon-4.png"}
//           ></Box>
//           <Typography variant="h5">RISK-FREE PROOF OF CONCEPT</Typography>
//           <Typography variant="subtitle1">
//             Arowana risk assessment simulation for offshore projects offers
//             comprehensive predictive risk analysis related to HSE and asset
//             integrity..
//           </Typography>
//         </Box>
//       </Slider>
//     </Box>
//   );
// };

// export default HeroCards;
import React, { Component } from "react";
import Slider from "react-slick";

export default class HeroCards extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <Box my={5}>
        <Typography
          variant="h4"
          fontWeight={"bold"}
          color={"#086AD8"}
          textAlign={"center"}
        >
          Why Choose Us
        </Typography>
        <Box my={4} pl={4} >
        <Slider {...settings}  >
          <Box
            sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)' , color : "#086AD8" }  }}
            component={Paper}
            elevation={4}
            borderRadius={3}
          >
            <Box
              component={"img"}
              src={"https://arowanasoft.com/img/icon-1.png"}
            ></Box>
            <Typography variant="h5" my={2} >
              Experience
            </Typography>
            <Typography variant="subtitle2" color={"black"}>
              Our team has gained a wealth of knowledge and skills through the
              process of digitally transforming over 600 offshore platforms and
              more than 75 onshore industrial facilities.
            </Typography>
          </Box>
          <Box
            sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)' ,color :  "#086AD8" }  }}
            component={Paper}
            elevation={3}
            borderRadius={3}
          >
            <Box
              component={"img"}
              src={"https://arowanasoft.com/img/icon-2.png"}
            ></Box>
            <Typography variant="h5" my={2} >
              Complete Digital Twinning
            </Typography>
            <Typography variant="subtitle2" color={"black"} >
              Our team is proficient in assisting with digital transformation
              efforts through in-house data acquisition, 3D modeling, data
              mining, and asset tagging.
            </Typography>
          </Box>
          <Box
            sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)' , color :  "#086AD8" }  }}
            component={Paper}
            elevation={3}
            borderRadius={3}
          >
            <Box
              component={"img"}
              src={"https://arowanasoft.com/img/icon-3.png"}
            ></Box>
            <Typography variant="h5" my={2} >Local Presence</Typography>
            <Typography variant="subtitle2" color={"black"} >
              We are a global company with a presence in many regions, which
              allows us to offer shorter turnaround times and maximum savings to
              our clients.
            </Typography>
          </Box>
          <Box
          sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)' , color :  "#086AD8" }  }}
          component={Paper}
          elevation={3}
          borderRadius={3}
        >
          <Box
            component={"img"}
            src={"https://arowanasoft.com/img/icon-4.png"}
          ></Box>
          <Typography variant="h5" my={2}  sx={{"&:hover": {color  : "#086AD8"}} }>RISK-FREE PROOF OF CONCEPT</Typography>
          <Typography variant="subtitle2" color={"black"}>
            Arowana risk assessment simulation for offshore projects offers
            comprehensive predictive risk analysis related to HSE and asset
            integrity..
          </Typography>
        </Box>
        <Box
           sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)'  ,color :  "#086AD8"}  }}
          component={Paper}
          elevation={3}
          borderRadius={3}
        >
          <Box
            component={"img"}
            src={"https://arowanasoft.com/img/icon-5.png"}
          ></Box>
          <Typography variant="h5" my={2} >PREDICTIVE ANALYTICS</Typography>
          <Typography variant="subtitle2" color={"black"} >
          Arowana field experience of 20 years, enable to offer accurate results and offers comprehensive predictive risk analysis related to HSE and asset integrity in risk assessment for offshore projects
          </Typography>
        </Box>
        <Box
          sx={{ maxWidth: 400, m :2, p: 2, minHeight: 280 , transition : 'all .3s ease' , "&:hover" : {transform : 'scale(1.04)' ,color :  "#086AD8" }  }}
          component={Paper}
          elevation={3}
          borderRadius={3}
        >
          <Box
            component={"img"}
            src={"https://arowanasoft.com/img/icon-6.png"}
          ></Box>
          <Typography variant="h5" my={2}  sx={{"&:hover": {color  : "#086AD8"}} }>INFORMATION SECURITY</Typography>
          <Typography variant="subtitle2" color={"black"}>
          Our emphasis on the security implications of a digital twin encourages us to offer the best-in-industry approach to security.
        </Typography>
        </Box>
        </Slider>
        </Box>

      </Box>
    );
  }
}
