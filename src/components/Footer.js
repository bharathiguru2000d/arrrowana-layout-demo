import React from 'react';
import { Container, Box, IconButton, Link, Typography } from '@mui/material';
import {  Twitter, Instagram, FacebookOutlined } from '@mui/icons-material';

const Footer = () => {
  return (
    <Box className="shadow" component="footer" py={5} >
      <Container>
        <Box display="flex" flexDirection="column" className="mx-auto py-5" style={{ width: '90%' }}>
          <Box display="flex" justifyContent="between" className="flex-wrap">
            <Box alignSelf="center">
              <Link href="/" className="d-flex align-items-center p-0 text-dark">
                <img alt="logo" src="https://arowanasoft.com/img/logo3.png" width="300px" />
                <Typography variant="h5" className="ms-3 font-weight-bold">
                  Arowana
                </Typography>
              </Link>
              <Box className="mt-5" display="flex">
                <IconButton  className="p-2">
                  <FacebookOutlined/>
                </IconButton>
                <IconButton  className="mx-3 p-2">
                <Twitter/>
                </IconButton>
                <IconButton  className="mx-3 p-2">
                <Instagram/>
                </IconButton>
              </Box>
            </Box>
            <Box px={10}>
              <Typography variant="h5" className="mb-4" style={{ fontWeight: '600' }}>
                Arowana
              </Typography>
              <Box display="flex" flexDirection="column" style={{ cursor: 'pointer' }}>
                <Link href="/">Resources</Link>
                <Link href="/">About Us</Link>
                <Link href="/">Contact</Link>
                <Link href="/">Blog</Link>
              </Box>
            </Box>
            <Box px={10}>
              <Typography variant="h5" className="mb-4" style={{ fontWeight: '600' }}>
                Products
              </Typography>
              <Box display="flex" flexDirection="column" style={{ cursor: 'pointer' }}>
                <Link href="/">Windframe</Link>
                <Link href="/">Loop</Link>
                <Link href="/">Contrast</Link>
              </Box>
            </Box>
            <Box px={10}>
              <Typography variant="h5" className="mb-4" style={{ fontWeight: '600', }}>
                Help
              </Typography>
              <Box display="flex" flexDirection="column" style={{ cursor: 'pointer' }}>
                <Link href="/">Support</Link>
                <Link href="/">Sign Up</Link>
                <Link href="/">Sign In</Link>
              </Box>
            </Box>
          </Box>
          <Typography variant="body2" className="text-center"  >
            &copy; Arowana, 2023. All rights reserved.
          </Typography>
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;