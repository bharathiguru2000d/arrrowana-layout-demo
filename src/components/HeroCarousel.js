import { Box, Typography } from "@mui/material";
import React from "react";
import Slider from "react-slick";
import One from "../utilities/one.jpg";
import Two from "../utilities/two.jpg";
import Three from "../utilities/three.jpg";
import Five from "../utilities/five.jpg";

const HeroCarousel = ()=> {



    const settings = {
      dots: false,
      arrows :true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    return (
      <Box>
        <Slider {...settings}>
        <Box
            sx={{
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              width: "100vw",
              height: "90vh",
              m: 0,
              p: 0,
              backgroundImage: `url(${Five})`,
            }}
          >
            <Box
              sx={{
                display: "flex",
                background: "rgba(0, 0, 0, 0.658)",
                color: "white",
                flexDirection: "column",
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Typography fontWeight={"bold"} variant={"h1"} my={2}>
              Building Information Modeling
              </Typography>
              <Typography variant="h6" sx={{ maxWidth: "700px" }}>
              We provide high-quality BIM services with state-of-the-art tools, providing your team with a comprehensive digital representation of your asset or facility.
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              width: "100vw",
              height: "90vh",
              m: 0,
              p: 0,
              backgroundImage: `url(${One})`,
            }}
          >
            <Box
              sx={{
                display: "flex",
                background: "rgba(0, 0, 0, 0.658)",
                color: "white",
                flexDirection: "column",
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Typography fontWeight={"bold"} variant={"h1"} my={2}>
                GIS & Utility Mapping
              </Typography>
              <Typography variant="h6" sx={{ maxWidth: "700px" }}>
                We provide Offshore Hydrographic Surveys and Geotechnical
                Services to an array of clients. We support every stage of
                nearshore and offshore projects through our comprehensive range
                of marine scientific services and help businesses to make
                informed decisions
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              width: "100vw",
              height: "90vh",
              backgroundImage: `url(${Two})`,
            }}
          >
            <Box
              sx={{
                display: "flex",
                background: "rgba(0, 0, 0, 0.658)",
                color: "white",
                flexDirection: "column",
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Typography fontWeight={"bold"} variant={"h1"} my={2}>
                3D Laser Scanning and Modeling
              </Typography>
              <Typography variant="h6" sx={{ maxWidth: "700px" }}>
                Our ATEX certified laser scanners perform work in explosive
                environment without affecting safety of people & plant.
                Automated processes reduce the scanning time and also help
                achieve high level of dimensional control for modeled spools.
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              width: "100vw",
              height: "90vh",
              backgroundImage: `url(${Three})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
            }}
          >
            <Box
              sx={{
                display: "flex",
                background: "rgba(0, 0, 0, 0.658)",
                color: "white",
                flexDirection: "column",
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Typography fontWeight={"bold"} variant={"h1"} my={2}>
                Structural Steel Detailing
              </Typography>
              <Typography variant="h6" sx={{ maxWidth: "700px" }}>
                Arowana is a professional managed engineering solutions provider
                with expertise in Design & detailed engineering. We provide
                fully detailed and accurate drawings with the assistance of the
                world’s most advanced powerful and flexible detailing softwares
                and tools.
              </Typography>
            </Box>
          </Box>
        </Slider>
      </Box>
    );
}

export default HeroCarousel;