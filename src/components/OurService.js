import { Box, Button, Grid, Typography } from "@mui/material";
import ViewInArIcon from "@mui/icons-material/ViewInAr";
import DetailsIcon from "@mui/icons-material/Details";
import DisplaySettingsIcon from '@mui/icons-material/DisplaySettings';
import CloudSyncIcon from '@mui/icons-material/CloudSync';
import ImageAspectRatioIcon from '@mui/icons-material/ImageAspectRatio';
import NewspaperIcon from '@mui/icons-material/Newspaper';
import ModelTrainingIcon from '@mui/icons-material/ModelTraining';
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import CellTowerIcon from '@mui/icons-material/CellTower';
import AddchartIcon from '@mui/icons-material/Addchart';
import AnalyticsIcon from '@mui/icons-material/Analytics';

const OurService = () => {
  return (
    <Box sx={{ backgroundColor: "#F8F8F8", minHeight: "70vh" , py:5}}>
      <Typography variant="h3" fontWeight={"bold"} textAlign={"center"} mb={5}>
        Our Services
      </Typography>
      <Grid
        container
        width={"100%"}
        height={"100%"}
        display={"flex"}
        justifyContent={"center"}
        alignItems={"center"}
      >
        <Grid
          my={2}
          item
          xs={7}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <ViewInArIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h5" mb={3} fontWeight={"bold"}>
                3D Laser Scanning
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                  We provide fully detailed and accurate drawings with the
                  assistance of the world’s most advanced powerful and flexible
                  detailing softwares and tools.{" "}
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Learn More
                </Button>
              </Box>
            </Box>
          </Box>

          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <DetailsIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
                Structural Steel Detailing
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                  We provide fully detailed and accurate drawings with the
                  assistance of the world’s most advanced powerful and flexible
                  detailing softwares and tools.{" "}
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <DisplaySettingsIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box sx={{height:"100%" }}>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
                Building Information Modelling
              </Typography>
              <Box
                sx={{display : "flex", flexDirection : "column" , justifyContent : "space-between" ,height : '150px' } }
              >
                <Typography variant="subtitle2">
                Building Information Modelling (BIM) is a transformative tool that utilises intelligent data to plan and design construction projects using contextual models.
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid
          item
          my={2}
          xs={7}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <CloudSyncIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h5" mb={3} fontWeight={"bold"}>
                2D Point Cloud
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                Land surveying is the detailed study or inspection, as by gathering information through observations, measurements in the field, questionnaires, or research of legal instruments.
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Learn More
                </Button>
              </Box>
            </Box>
          </Box>

          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <ModelTrainingIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
               Cyclone Modelling
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                Cyclone Model is the leading standalone software for analyzing laser scan data and turning it into useful deliverables..{" "}
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <DisplaySettingsIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box sx={{height:"100%" }}>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
                As Build Modeling
              </Typography>
              <Box
                sx={{display : "flex", flexDirection : "column" , justifyContent : "space-between" ,height : '180px' } }
              >
                <Typography variant="subtitle2">
                BIM is a "digital representation" of a building's physical and functional properties and characteristics. BIM goes further than just the building's physical appearance.</Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid
          item
          my={2}
          xs={7}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <MapsHomeWorkIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h5" mb={3} fontWeight={"bold"}>
                GIS for Utility Mapping
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                GIS surveying go hand-in-hand. When drone technology, such as UAVs (unmanned aerial vehicles) are used with GIS platforms, it makes it easier for drones to report.
                 </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Learn More
                </Button>
              </Box>
            </Box>
          </Box>

          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <NewspaperIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
               Lidar Data Visualization and Aanlayzatioin
              </Typography>
              <Box
                display={"flex"}
                height={"140px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                Lidar, which stands for Light Detection and Ranging, is a remote sensing method that uses light in the form of a pulsed laser </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <ImageAspectRatioIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box >
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
                Photogramettry
              </Typography>
              <Box
                sx={{display : "flex", flexDirection : "column" , justifyContent : "space-between" , height: '170px' } }
              >
                <Typography variant="subtitle2">
                It provides professional mapping services with on-time delivery schedules and in a cost effective process. </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid
          item
          xs={7}

          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <CellTowerIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h5" mb={3} fontWeight={"bold"}>
                Telecom Domain Surveying
              </Typography>
              <Box
                display={"flex"}
                height={"140px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                We utilizes the latest technological tools available to ensure the efficient collection and transfer of field data to our in-house design and engineering teams.
                </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>

          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <AddchartIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
               Drone Surveying
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                It is the process used to determine the exact position of objects on the Earth's surface (whether natural or artificial) by collecting, evaluating and recording various data on the ground.
               </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
          <Box
            display={"flex"}
            alignItems={"flex-start"}
            justifyContent={"flex-start"}
            minWidth={"400px"}
            p={2}
            height={"280px"}
            fontWeight={"bold"}
            sx={{
              bgcolor: "#F8F8F8",
              transition: "all .6s ease",
              borderRadius: 1,
              "&:hover": {
                bgcolor: "white",
                transform: "scale(1.01) translatey(-5px)",
                boxShadow: "1px 1px 14px .2px rgba(0, 0, 0, 0.529)",
              },
            }}
          >
            <AnalyticsIcon sx={{ fontSize: "50px", mr: 3 }} />
            <Box>
              <Typography variant="h6" mb={3} fontWeight={"bold"}>
            Surveying
              </Typography>
              <Box
                display={"flex"}
                height={"180px"}
                flexDirection={"column"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle2">
                Visualization through maps plays a vital role in Urban Planning, Utility Maintenance , LandUse planning and Transportation. Kashyap GeoTech, provides the best DGPS survey.
               </Typography>
                <Button variant="outlined" sx={{ width: "150px" }}>
                  Read More
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default OurService;
