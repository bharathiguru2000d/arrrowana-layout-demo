import { Box } from '@mui/material';
import './App.css';
import Contact from './components/Contact';
import Footer from './components/Footer';
import Header from './components/Header';
import HeroCards from './components/HeroCards';
import HeroCarousel from './components/HeroCarousel';
import MainHero from './components/MainHero';
import OurService from './components/OurService';

function App() {
  return (
    <div className="App">
      <Header />
      <Box mt={12} >
      <HeroCarousel />
      <HeroCards />
      <MainHero />
      <OurService />
      <Contact />
      <Footer />
      </Box>
    </div>
  );
}

export default App;
